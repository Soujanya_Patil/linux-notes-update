
"""
$ tail -n +2 sort_demo_scores.csv | \
> cut -d, -f6 | \
> sort | \
> uniq -c | \
> sort -nr | \
> awk 'BEGIN {print "Grade  Count"} {printf "%5s  %5d\n", $2, $1; total += $1} END {printf "%5s  %5d\n", "Total", total}'

Grade  Count
   07     69
   06     69
   03     57
   05     54
   04     51
Total    300
"""

score_file = "sort_demo_scores.csv"
stats = dict()
total = 0

with open(score_file, 'r') as fh:
    hdr = fh.readline()
    for rec in fh:
        fields = rec.strip().split(",")
        grade = fields[5]

        if grade not in stats:
            stats[grade] = 0

        total += 1
        stats[grade] = stats.get(grade, 0) + 1

print("Grade  Count")
for grade in sorted(stats):
    print(f"{grade:>5}  {stats[grade]:5}")
print(f"Total  {total:5}")
