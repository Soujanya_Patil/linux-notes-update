from faker import Faker

import random

MAX_RECS = 100000
f = Faker()

grades = [ f"0{i}" for i in range(3, 9)]

cfh = open("split_demo.csv", "w")
tfh = open("split_demo.txt", "w")

for i in range(MAX_RECS):
    rec = f"S{i:07}"
    last_name = f.last_name()
    gender = random.choice(['M', 'F'])
    
    if gender == 'M':
        first_name = f.first_name_male()
    else:
        first_name = f.first_name_female()

    grade = random.choice(grades)
    print(f"{rec} {last_name:15} {first_name:15} {gender} {grade:2}", file=tfh)
    print(rec, last_name, first_name, gender, grade, "Z", file=cfh, sep=",")

cfh.close()
tfh.close()
