from faker import Faker

f = Faker()

with open("paste_address.txt", "w") as fh:
    for _ in range(10):
        print(f.name(), f.address(), sep="\n", file=fh)
