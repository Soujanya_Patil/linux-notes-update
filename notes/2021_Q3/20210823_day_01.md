# Linux: Getting Started

## Topics
1. OS, History and Architecture
1. Command Syntax
1. File System
1. IO Redirection
1. Permission
1. Data Manipulation
1. Regular Expressions
1. Vi editor
1. Text Processing: grep, sed, awk
1. Process Management & Job Scheduling
1. Shell Scripting

## Introduction


## Operating System: From Beginning till now
1. Users have to control all aspects of running application
2. Allocating resources such as processing time, memory etc..
3. Libraries or reusable code were created to automate some of these tasks
4. Jobs were still run one-at-a-time; job queues were introduced
5. Monitoring CPU time and other resource usages for accounting (costs)
6. All the above and much more combined to form an "Operating System"
7. OS: hardware management, job scheduling, resource allocation and monitoring, libraries, commands and utilities, CLI and GUI
8. The core of the OS is called Kernel that handles what the original OSes did
9. CLI aka Shell sits between kernel and user, application to facilitate interactions
10. Compilers, Assemblers, Text editors and other utilities help building applications 

## Operating Systems & Linux
1. First OSes were built for mainframes 
2. IBM OS/360, OS/370 from the 60s continue to live as z/OS
3. Initial focus on "Batch processing" and later "Time Sharing" or Interactive processing
4. UNIVAC, Control Data Corp, GE/Honeywell built own OS 
5. With the advent of minicomputers, another wave of OS such as IBM's OS/400
6. OS/400 later become IBM iSeries and still be used today along with zSeries for mainframes
7. MIT, GE, AT&T developed "Multics" which is the predecessor of "Unix"
8. "Multics" development was discorded due to complexisty issues
9. Ken Thomposon from "Multics" team developed an OS to port and play space travel game available on Multics
10. The OS gained popularity in AT&T and named as "UNICS" that later become "UNIX"
11. Thompson originally wrote "UNIX" in assembly language for PDP-7
12. In the meantime, Dennis Ritichie developed the "Portable" language called "C"
13. Thomposon rewrote "UNIX" in C for PDP-11 system
14. Due to laws and regulations, AT&T could not sell UNIX so they gave it away to universities and labs 
15. UNIX was widely adopted and may brilliant minds contributed to make it a superpower OS 
16. Two variants of UNIX emerged: System V from AT&T and BSD from University of California, Berkley
17. More Unix-like OS were built; IBM AIX, HP-UX, Solaris, SunOS, Minix to name a few
18. UNIX trademark was acquired by the "Open Group"
19. FSF started delveloping GNU which is the set of commands and utilities present in UNIX as an open-source alternative
20. Linus Torvalds build a Unix-like Kernel from the scratch that became an open source Linux Kernel
21. Linux Kernel and GNU utilities combined to become Linux OS
22. Since it is free, many versions of Linux with additional features have been created and they are called "Linux Distributions"
23. Red Hat, Fedora, Debian, CentOS, Ubuntu, Mint, KaliLinux to name a few desktop versions and iOS, Android for mobiles
24. MacOS was built using BSD version of UNIX with Apple's proprieary applications.

## Flavors of Unix

**Need to add the image**

## Unix Philosophy
1. Similicity
1. Modularity
1. Portability
1. Treat Data as text
1. Treat commands as filter

**For more info:** [Unix Philosophy @ Wikipedia](https://en.wikipedia.org/wiki/Unix_philosophy)

## Architecture
1. **Kernel** forms the core of the OS; sole interface to the hardware
1. **Shell** protects the kernel, serve as an interface between users/apps and kernel
1. **Applications, Commands and Utilities** runs using hardware resources via shell and kernel

## Hands-on
1. Linux or MacOS desktop
1. Linux on Virtual Machine
1. Access to Linux Server
1. Emulators like MobaXterm or Cygwin
1. Git Bash for Windows
1. Web-based apps like **Google cloud shell**, **replit.com**